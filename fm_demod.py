#!/usr/bin/env python
"""Demodulates an FM radio station sample to raw audio

Usage example:

    # Record some RF samples
    $> rtl_sdr -s 2.4M -f 102.1M -n 24000000 rf_samples.cu8
    # Demod the samples
    $> ./fm_demod.py rf_samples.cu8 audio_samples.ci16
    # Play back the audio
    $> mplayer -cache 1024 -quiet -rawaudio samplesize=2:channels=1:rate=48000 -demuxer rawaudio audio_samples.ci16

Notes:
    - Requires fixed 2.4MHz RF sampling rate
    - Requires RF sample input file as uint8 complex samples
    - Assumes 75us tau value for deemphasis
    - Audio sample output is int16 samples
"""

import argparse

import numpy as np

RF_SAMPLE_RATE = 2.4e6
RF_CUTOFF = 200e3
RF_DECIMATION = 10
AUDIO_DECIMATION = 5
AUDIO_CUTOFF = 16e3
NUM_TAPS = 201
DEEMPHASIS_TAU = 75e-6


def read_input(input_file):
    """Reads RF samples from an input file.

    Args:
        input_file (pathlib.Path): File of RF samples as complex uint8 samples.

    Returns:
        numpy.ndarray: 64-bit complex samples in a Numpy array.
    """
    data = np.fromfile(input_file, dtype=np.uint8)
    data = data.astype(np.float32)
    data = data / (255.0 / 2.0) - 1.0
    return data.view(np.complex64)


def write_output(output_file, samples):
    """Writes audio samples to an output file.

    Args:
        output_file (pathlib.Path): File to write audio samples to. Writes as int16 values.
    """
    samples *= (1 << 15) - 1
    samples = samples.astype(np.int16)
    samples.tofile(output_file)


def build_low_pass_filter(sample_freq, cutoff_freq, num_taps):
    """Construct a set of low pass filter taps.

    Args:
        sample_freq (float): Sample frequency of the input signal.
        cutoff_freq (float): Cutoff frequency of the low pass filter. Must be <= sample_freq / 2.
        num_taps (int): Number of taps to include in the filter. Must be odd.

    Returns:
        numpy.ndarray: 1-D NumPy array of normalized low pass filter taps
    """
    cutoff_rate = cutoff_freq / sample_freq

    assert 0 <= cutoff_rate <= 0.5
    assert num_taps % 2 != 0

    points = np.arange(num_taps) - num_taps // 2

    window = np.hamming(num_taps)
    sinc = np.sinc(2 * cutoff_rate * points)
    h = sinc * window
    normalized = h / h.sum()
    return normalized.astype(np.float32)


def demod(signal):
    """Demodulates an FM signal.

    Args:
        signal (numpy.ndarray): RF samples as 64-bit complex values.

    Returns:
        numpy.ndarray: Demodulated samples as normalized 32-bit float values.
    """
    angles = np.angle(signal)
    angles = np.unwrap(angles)
    diffs = np.convolve(angles, [1, -1], 'valid')
    diffs /= max(abs(diffs.min()), diffs.max())
    return diffs.astype(np.float32)


def deemphasis(signal, sample_freq):
    """Applies the FM deemphasis filter

    Args:
        signal (numpy.ndarray): Audio samples
        sample_freq (int): Sample rate of the audio signal

    Returns:
        numpy.ndarray: Filtered input signal
    """
    dt = 1.0 / sample_freq
    alpha = dt / (DEEMPHASIS_TAU + dt)
    deemphasized = np.convolve(signal, [1 - alpha, alpha], 'valid')
    return deemphasized


def _parse_args():
    parser = argparse.ArgumentParser(
        description='Demodulate an FM radio station sample to raw audio',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('input_file', help='Input file of RF samples.')
    parser.add_argument('output_file', help='Output file of audio samples.')
    return parser.parse_args()


def main():
    args = _parse_args()

    # Build filters
    rf_filter = build_low_pass_filter(RF_SAMPLE_RATE, RF_CUTOFF, NUM_TAPS)
    audio_filter = build_low_pass_filter(RF_SAMPLE_RATE // RF_DECIMATION,
                                         AUDIO_CUTOFF, NUM_TAPS)

    # Read input
    rf_samples = read_input(args.input_file)

    # Demod
    filtered_rf_samples = np.convolve(rf_samples, rf_filter, 'valid')
    decimated_rf_samples = filtered_rf_samples[::RF_DECIMATION]
    audio_samples = demod(decimated_rf_samples)
    filtered_audio_samples = np.convolve(audio_samples, audio_filter, 'valid')
    decimated_audio_samples = filtered_audio_samples[::AUDIO_DECIMATION]
    deemphasized_audio_samples = deemphasis(
        decimated_audio_samples,
        RF_SAMPLE_RATE // RF_DECIMATION // AUDIO_DECIMATION)

    # Write output
    write_output(args.output_file, deemphasized_audio_samples)


if __name__ == '__main__':
    main()
